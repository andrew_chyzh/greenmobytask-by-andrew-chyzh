package com.andrew_chyzh.greenmobytask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Logic {
    private Map<String, String> sortedResultsMap = new TreeMap<String, String>();
    private String[] results;

    private static Logic ourInstance = new Logic();

    public static Logic getInstance() {
        return ourInstance;
    }

    private Logic() {
    }

    public static String getRequest(String url){
        InputStream inputStream;
        String result = "";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(new HttpGet(url));
            inputStream = response.getEntity().getContent();
            result = convertInputStream(inputStream);
        } catch (Exception e) {
        }
        return result;
    }

    public void complete(String result){
        try {
            JSONObject jsonResult = parseToJson(result);
            sortResult(jsonResult);
            showResult();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject parseToJson(String result) throws JSONException {
        result = "{keys:" + result + "}";
        JSONObject jsonObject;
        jsonObject = new JSONObject(result);
        return jsonObject;
    }

    private void sortResult(JSONObject jObject) throws JSONException {
        JSONObject keys = jObject.getJSONObject("keys");

        Iterator iterator = keys.keys();
        while(iterator.hasNext()){
            String key = (String)iterator.next();
            String value = keys.getString(key);
            sortedResultsMap.put(key, value);
        }
    }

    private static String convertInputStream(InputStream inputStream) throws IOException, JSONException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String strResult = "";

        while((line = bufferedReader.readLine()) != null)
            strResult += line;
        inputStream.close();
        return strResult;
    }

    private void showResult(){
        String[] results = new String[sortedResultsMap.size()];
        int ind = 0;

        for(Map.Entry<String, String> entry: sortedResultsMap.entrySet()){
            results[ind] = entry + "";
            ++ind;
        }
        setResults(results);
    }

    public String[] getResults() {
        return results;
    }

    public void setResults(String[] results) {
        this.results = results;
    }
}
