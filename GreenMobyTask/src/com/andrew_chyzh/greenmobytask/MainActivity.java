package com.andrew_chyzh.greenmobytask;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	static ListView listView;

    String url = "http://echo.jsontest.com/key1/value1/key2/value2/key3/value3/key4/value4/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listView = (ListView)findViewById(R.id.listView);
		
		HttpAsyncTask asyncTask = new HttpAsyncTask();
        asyncTask.execute(url);		
	}
	
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String[] urls) {
            return Logic.getRequest(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            Logic.getInstance().complete(result);
            showResults();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void showResults(){
        String[] results = Logic.getInstance().getResults();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, results);
        listView.setAdapter(adapter);
    }
}
